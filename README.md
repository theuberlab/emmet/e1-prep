# E1 Prep

A place to organize some notes about getting up to speed as a support/ops Engineer lvl1.

# Skills to Develop for the Support Gig
terminal usage, incident response, documentation, ability to search slack/man/google/etc and find relevant information

Soft Skills: Patience, empathy, and quick thinking.
General knowledge of networking, Docker, Linux, and git.

A strong desire to learn.
Know a computer language (any language) decently well.
Know how to build something simple and deploy it.
Understand command line basics.
Understand git.

## Above Turned Into Practicable Tasks

* Troubleshooting
    * Let's get together and I'll give you some troubleshooting scenarios.
* Emphasize your ability to learn quickly.
* Have some knowledge of docker
    * Be able to explain the difference between containers and virtualization
    * Familiarize self with docker commands ps, info, run
    * Know how to build a docker container
* Have some basic networking knowledge
    * What's a packet
    * What is TCP
    * What is IP
    * Know the OSI model
    * Know that it's different from the TCP model
    * Be able to explain what a subnet is
    * Be able to define CIDR (don't worry too hard about being able to do it off the top of your head.)
    * Be able to explain in detail what happens from when you type www.example.com into your browser and hit enter to when the page is rendered
* Be able to find answers
    * Search google
    * Search gitlab
    * Search confluence
* Have some Linux skills
    * Be able to navigate (cd, ls)
    * Be able to read and edit files (cat, vi, grep)
* Basic git skills
    * Know what a repo is
    * Know how to clone one
    * Know how to create a branch 
    * Know how to create an MR(PR)

# Troubleshooting Scnenarios
These are some hypothetical scenarios that require troubleshooting.

Let's run through them to develop the troubleshooting muscles.


## Onboard a Customer to Artifactory
You get a ticket requesting access to Artifactory. You've never heard of Artifactory but apparently your team supports it. How do you figure out what to do?

## Funny Sound While Driving
You hear a noise coming from the front of your car while you’re driving down the road with your music turned way up. How do you start?

## User Can't Print
User can't print. Where do you start?

## User's Pipeline is Failing
Customer complains that they are trying to use the standard pipeline but it's failing. What do you do?
